#! /usr/bin/env python

import rospy
import sys
import copy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import actionlib
import math
from tf.transformations import quaternion_from_euler


class Ur5Moveit:

    # Constructor
    def __init__(self):

        rospy.init_node('node_eg4_set_joint_angles', anonymous=True)

        self._planning_group = "ur5_1_planning_group"
        self._commander = moveit_commander.roscpp_initialize(sys.argv)
        self._robot = moveit_commander.RobotCommander()
        self._scene = moveit_commander.PlanningSceneInterface()
        self._group = moveit_commander.MoveGroupCommander(self._planning_group)
        self._display_trajectory_publisher = rospy.Publisher(
            '/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)

        self._exectute_trajectory_client = actionlib.SimpleActionClient(
            'execute_trajectory', moveit_msgs.msg.ExecuteTrajectoryAction)
        self._exectute_trajectory_client.wait_for_server()

        self._planning_frame = self._group.get_planning_frame()
        self._eef_link = self._group.get_end_effector_link()
        self._group_names = self._robot.get_group_names()
        self._box_name = ''

        # Current State of the Robot is needed to add box to planning scene
        self._curr_state = self._robot.get_current_state()

        rospy.loginfo(
            '\033[94m' + "Planning Group: {}".format(self._planning_frame) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "End Effector Link: {}".format(self._eef_link) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "Group Names: {}".format(self._group_names) + '\033[0m')

        rospy.loginfo('\033[94m' + " >>> Ur5Moveit init done." + '\033[0m')

    def go_to_pose(self, arg_pose):

        pose_values = self._group.get_current_pose().pose
        rospy.loginfo('\033[94m' + ">>> Current Pose:" + '\033[0m')
        rospy.loginfo(pose_values)

        self._group.set_pose_target(arg_pose)
        flag_plan = self._group.go(wait=True)  # wait=False for Async Move

        pose_values = self._group.get_current_pose().pose
        rospy.loginfo('\033[94m' + ">>> Final Pose:" + '\033[0m')
        rospy.loginfo(pose_values)

        list_joint_values = self._group.get_current_joint_values()
        rospy.loginfo('\033[94m' + ">>> Final Joint Values:" + '\033[0m')
        rospy.loginfo(list_joint_values)

        if (flag_plan == True):
            rospy.loginfo(
                '\033[94m' + ">>> go_to_pose() Success" + '\033[0m')
        else:
            rospy.logerr(
                '\033[94m' + ">>> go_to_pose() Failed. Solution for Pose not Found." + '\033[0m')

        return flag_plan

    # Destructor
    def __del__(self):
        moveit_commander.roscpp_shutdown()
        rospy.loginfo(
            '\033[94m' + "Object of class Ur5Moveit Deleted." + '\033[0m')


class gripper:

    # Constructor
    def __init__(self):


        self._planning_group = "gripper"
        self._commander = moveit_commander.roscpp_initialize(sys.argv)
        self._robot = moveit_commander.RobotCommander()
        self._scene = moveit_commander.PlanningSceneInterface()
        self._group = moveit_commander.MoveGroupCommander(self._planning_group)
        self._display_trajectory_publisher = rospy.Publisher(
            '/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)

        self._exectute_trajectory_client = actionlib.SimpleActionClient('execute_trajectory', moveit_msgs.msg.ExecuteTrajectoryAction)
        self._exectute_trajectory_client.wait_for_server()

        self._planning_frame = self._group.get_planning_frame()
        self._eef_link = self._group.get_end_effector_link()
        self._group_names = self._robot.get_group_names()


        rospy.loginfo(
            '\033[94m' + "Planning Group: {}".format(self._planning_frame) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "End Effector Link: {}".format(self._eef_link) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "Group Names: {}".format(self._group_names) + '\033[0m')

        rospy.loginfo('\033[94m' + " >>> Ur5Moveit init done." + '\033[0m')

    def go_to_predefined_pose(self, arg_pose_name):
        rospy.loginfo('\033[94m' + "Going to Pose: {}".format(arg_pose_name) + '\033[0m')
        self._group.set_named_target(arg_pose_name)
        plan = self._group.plan()
        goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
        goal.trajectory = plan
        self._exectute_trajectory_client.send_goal(goal)
        self._exectute_trajectory_client.wait_for_result()
        rospy.loginfo('\033[94m' + "Now at Pose: {}".format(arg_pose_name) + '\033[0m')

    # Destructor
    def __del__(self):
        moveit_commander.roscpp_shutdown()
        rospy.loginfo(
            '\033[94m' + "Object of class Ur5Moveit Deleted." + '\033[0m')


'''class WorldObject: Class to create world object a helper functions like pick_object, drop_im_box
    Required parameters:
        name -> type:string -> name of the object
        position -> type:list -> list containing coords of the object
        orientation -> type:list -> list containg orientation of the object in euler'''
class WorldObject:

    # constructor
    def __init__(self, name, position, orientation):
        self.name = name
        self.position = position
        self.orientation = quaternion_from_euler(orientation[0], orientation[1], orientation[2], axes='sxyz')
        self.ur5 = Ur5Moveit()
        self.gr = gripper()

    '''pick_object(self) function: function to pick the required object using the ur5 arm and gripper
        No parameters required'''
    def pick_object(self):
        # create geometry pose object to take ur5 arm over the required object
        ur5_pose = geometry_msgs.msg.Pose()
        ur5_pose.position.x = self.position[0] 
        ur5_pose.position.y = self.position[1] 
        ur5_pose.position.z = self.position[2] + 0.15
        ur5_pose.orientation.x = self.orientation[0]
        ur5_pose.orientation.y = self.orientation[1]
        ur5_pose.orientation.z = self.orientation[2]
        ur5_pose.orientation.w = self.orientation[3]

        # open the gripper before picking the object
        self.gr.go_to_predefined_pose("open")
        self.ur5.go_to_pose(ur5_pose)
        rospy.sleep(1)

        # descend the arm to actual object position
        ur5_pose.position.z = self.position[2] 
        self.ur5.go_to_pose(ur5_pose)

        # close the gripper to hold the object
        self.gr._scene.remove_world_object(self.name)
        self.gr.go_to_predefined_pose("close")

    '''drop_in_box(self, box_color): function to drop the object in the required box
        Parameters:
            box_color -> type:string -> color of the box in which the object is to be dropped'''
    def drop_in_box(self, box_color):
        # check in which box the object is to be dropped
        if box_color == "red":
            quaternion = quaternion_from_euler(-1.57, -0.02, -0.9, axes='sxyz')
            x = -0.1 
            y = 0.675
            z = 1.1  

        elif box_color == "blue":
            quaternion = quaternion_from_euler(-1.57, -0.02, -0.9, axes='sxyz')
            x = -0.007 
            y = -0.69 
            z = 1.2 

        # create a geometry message with box pose and take the ur5 arm to it
        ur5_pose = geometry_msgs.msg.Pose()
        ur5_pose.position.x = x
        ur5_pose.position.y = y 
        ur5_pose.position.z = z
        ur5_pose.orientation.x = quaternion[0]
        ur5_pose.orientation.y = quaternion[1]
        ur5_pose.orientation.z = quaternion[2]
        ur5_pose.orientation.w = quaternion[3]
        self.ur5.go_to_pose(ur5_pose)
        rospy.sleep(1)

        # open the gripper to drop the object in box
        self.gr.go_to_predefined_pose("open")


def main():

    # create world objects as instances of WorldObject class
    soap_0 = WorldObject('soap.dae_0', [0.56, -0.03, 0.82], [-1.57, 0, -0.9]) 
    soap_1 = WorldObject('soap.dae_2', [0.45, 0.22, 0.82], [-1.58732042002, 0.00999523051594, 2.32011439703])
    biscuit = WorldObject('biscuits.dae_0', [0.556, -0.262, 0.873], [-1.66, 0.0178570746973, 2.8712563524]) 

    # pick up and then drop soap_0 object in red box
    soap_0.pick_object()
    soap_0.drop_in_box("red")

    # pick up and then drop soap_1 object in blue box
    soap_1.pick_object()
    soap_1.drop_in_box("blue")

    # pick up and then drop biscuits object in blue box
    biscuit.pick_object()
    biscuit.drop_in_box("blue")


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass

