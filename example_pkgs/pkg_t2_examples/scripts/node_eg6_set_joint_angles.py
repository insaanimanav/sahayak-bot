#! /usr/bin/env python

import rospy
import sys
import copy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import actionlib
import math
import tf.transformations



class Ur5Moveit:

    # Constructor
    def __init__(self):

        rospy.init_node('node_eg4_set_joint_angles', anonymous=True)

        self._planning_group = "ur5_1_planning_group"
        self._commander = moveit_commander.roscpp_initialize(sys.argv)
        self._robot = moveit_commander.RobotCommander()
        self._scene = moveit_commander.PlanningSceneInterface()
        self._group = moveit_commander.MoveGroupCommander(self._planning_group)
        self._display_trajectory_publisher = rospy.Publisher(
            '/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)

        self._exectute_trajectory_client = actionlib.SimpleActionClient(
            'execute_trajectory', moveit_msgs.msg.ExecuteTrajectoryAction)
        self._exectute_trajectory_client.wait_for_server()

        self._planning_frame = self._group.get_planning_frame()
        self._eef_link = self._group.get_end_effector_link()
        self._group_names = self._robot.get_group_names()
        self._box_name = ''

        # Current State of the Robot is needed to add box to planning scene
        self._curr_state = self._robot.get_current_state()

        rospy.loginfo(
            '\033[94m' + "Planning Group: {}".format(self._planning_frame) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "End Effector Link: {}".format(self._eef_link) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "Group Names: {}".format(self._group_names) + '\033[0m')

        rospy.loginfo('\033[94m' + " >>> Ur5Moveit init done." + '\033[0m')

    def go_to_pose(self, arg_pose):

        pose_values = self._group.get_current_pose().pose
        rospy.loginfo('\033[94m' + ">>> Current Pose:" + '\033[0m')
        rospy.loginfo(pose_values)

        self._group.set_pose_target(arg_pose)
        flag_plan = self._group.go(wait=True)  # wait=False for Async Move

        pose_values = self._group.get_current_pose().pose
        rospy.loginfo('\033[94m' + ">>> Final Pose:" + '\033[0m')
        rospy.loginfo(pose_values)

        list_joint_values = self._group.get_current_joint_values()
        rospy.loginfo('\033[94m' + ">>> Final Joint Values:" + '\033[0m')
        rospy.loginfo(list_joint_values)

        if (flag_plan == True):
            rospy.loginfo(
                '\033[94m' + ">>> go_to_pose() Success" + '\033[0m')
        else:
            rospy.logerr(
                '\033[94m' + ">>> go_to_pose() Failed. Solution for Pose not Found." + '\033[0m')

        return flag_plan


    def wait_for_state_update(self, box_is_known=False, box_is_attached=False, timeout=4):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        box_name = self._box_name
        scene = self._scene

        ## BEGIN_SUB_TUTORIAL wait_for_scene_update
        ##
        ## Ensuring Collision Updates Are Receieved
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        ## If the Python node dies before publishing a collision object update message, the message
        ## could get lost and the box will not appear. To ensure that the updates are
        ## made, we wait until we see the changes reflected in the
        ## ``get_known_object_names()`` and ``get_known_object_names()`` lists.
        ## For the purpose of this tutorial, we call this function after adding,
        ## removing, attaching or detaching an object in the planning scene. We then wait
        ## until the updates have been made or ``timeout`` seconds have passed
        start = rospy.get_time()
        seconds = rospy.get_time()
        while (seconds - start < timeout) and not rospy.is_shutdown():
          # Test if the box is in attached objects
          attached_objects = scene.get_attached_objects([box_name])
          is_attached = len(attached_objects.keys()) > 0

          # Test if the box is in the scene.
          # Note that attaching the box will remove it from known_objects
          is_known = box_name in scene.get_known_object_names()

          # Test if we are in the expected state
          if (box_is_attached == is_attached) and (box_is_known == is_known):
            return True

          # Sleep so that we give other threads time on the processor
          rospy.sleep(0.1)
          seconds = rospy.get_time()

        # If we exited the while loop without returning then we timed out
        return False
        ## END_SUB_TUTORIAL


    def add_box(self, timeout=4):
       # Copy class variables to local variables to make the web tutorials more clear.
       # In practice, you should use the class variables directly unless you have a good
       # reason not to.
       box_name = self._box_name
       scene = self._scene

       ## BEGIN_SUB_TUTORIAL add_box
       ##
       ## Adding Objects to the Planning Scene
       ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
       ## First, we will create a box in the planning scene at the location of the left finger:
       box_pose = geometry_msgs.msg.PoseStamped()
       box_pose.header.frame_id = "gripper_finger1_finger_tip_link"
       box_pose.pose.orientation.w = 1.0
       box_name = "box"
       scene.add_box(box_name, box_pose, size=(0.1, 0.1, 0.1))

       ## END_SUB_TUTORIAL
       # Copy local variables back to class variables. In practice, you should use the class
       # variables directly unless you have a good reason not to.
       self.box_name=box_name
       return self.wait_for_state_update(box_is_known=True, timeout=timeout)


    def attach_box(self, box_name, timeout=4):
      # Copy class variables to local variables to make the web tutorials more clear.
      # In practice, you should use the class variables directly unless you have a good
      # reason not to.
      self._box_name =  box_name
      robot = self._robot
      scene = self._scene
      eef_link = self._eef_link
      group_names = self._group_names

      ## BEGIN_SUB_TUTORIAL attach_object
      ##
      ## Attaching Objects to the Robot
      ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      ## Next, we will attach the box to the Panda wrist. Manipulating objects requires the
      ## robot be able to touch them without the planning scene reporting the contact as a
      ## collision. By adding link names to the ``touch_links`` array, we are telling the
      ## planning scene to ignore collisions between those links and the box. For the Panda
      ## robot, we set ``grasping_group = 'hand'``. If you are using a different robot,
      ## you should change this value to the name of your end effector group name.
      grasping_group = 'gripper'
      touch_links = robot.get_link_names(group=grasping_group)
      scene.attach_box(eef_link, box_name, touch_links=touch_links)
      ## END_SUB_TUTORIAL
      return self.wait_for_state_update(box_is_attached=True, box_is_known=False, timeout=timeout)


    def detach_box(self, timeout=4):
      # Copy class variables to local variables to make the web tutorials more clear.
      # In practice, you should use the class variables directly unless you have a good
      # reason not to.
      box_name = self._box_name
      scene = self._scene
      eef_link = self._eef_link

      ## BEGIN_SUB_TUTORIAL detach_object
      ##
      ## Detaching Objects from the Robot
      ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      ## We can also detach and remove the object from the planning scene:
      scene.remove_attached_object(eef_link, name=box_name)
      ## END_SUB_TUTORIAL

      # We wait for the planning scene to update.
      return self.wait_for_state_update(box_is_known=True, box_is_attached=False, timeout=timeout)

    def remove_box(self, timeout=4):
      # Copy class variables to local variables to make the web tutorials more clear.
      # In practice, you should use the class variables directly unless you have a good
      # reason not to.
      box_name = self._box_name
      scene = self._scene

      ## BEGIN_SUB_TUTORIAL remove_object
      ##
      ## Removing Objects from the Planning Scene
      ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      ## We can remove the box from the world.
      scene.remove_world_object(box_name)


    # Destructor
    def __del__(self):
        moveit_commander.roscpp_shutdown()
        rospy.loginfo(
            '\033[94m' + "Object of class Ur5Moveit Deleted." + '\033[0m')

class gripper:

    # Constructor
    def __init__(self):


        self._planning_group = "gripper"
        self._commander = moveit_commander.roscpp_initialize(sys.argv)
        self._robot = moveit_commander.RobotCommander()
        self._scene = moveit_commander.PlanningSceneInterface()
        self._group = moveit_commander.MoveGroupCommander(self._planning_group)
        self._display_trajectory_publisher = rospy.Publisher(
            '/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)

        self._exectute_trajectory_client = actionlib.SimpleActionClient('execute_trajectory', moveit_msgs.msg.ExecuteTrajectoryAction)
        self._exectute_trajectory_client.wait_for_server()

        self._planning_frame = self._group.get_planning_frame()
        self._eef_link = self._group.get_end_effector_link()
        self._group_names = self._robot.get_group_names()


        rospy.loginfo(
            '\033[94m' + "Planning Group: {}".format(self._planning_frame) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "End Effector Link: {}".format(self._eef_link) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "Group Names: {}".format(self._group_names) + '\033[0m')

        rospy.loginfo('\033[94m' + " >>> Ur5Moveit init done." + '\033[0m')

    def go_to_predefined_pose(self, arg_pose_name):
        rospy.loginfo('\033[94m' + "Going to Pose: {}".format(arg_pose_name) + '\033[0m')
        self._group.set_named_target(arg_pose_name)
        plan = self._group.plan()
        goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
        goal.trajectory = plan
        self._exectute_trajectory_client.send_goal(goal)
        self._exectute_trajectory_client.wait_for_result()
        rospy.loginfo('\033[94m' + "Now at Pose: {}".format(arg_pose_name) + '\033[0m')
    

    # Destructor
    def __del__(self):
        moveit_commander.roscpp_shutdown()
        rospy.loginfo(
            '\033[94m' + "Object of class Ur5Moveit Deleted." + '\033[0m')

quaternion = tf.transformations.quaternion_from_euler(-math.pi/2, 0, -0.9, axes='sxyz')
def main():

    ur5 = Ur5Moveit()
    gr = gripper()

    ur5_pose_1 = geometry_msgs.msg.Pose()
    ur5_pose_1.position.x = 0.56
    ur5_pose_1.position.y = -0.03
    ur5_pose_1.position.z = 1.0
    ur5_pose_1.orientation.x = quaternion[0]
    ur5_pose_1.orientation.y = quaternion[1]
    ur5_pose_1.orientation.z = quaternion[2]
    ur5_pose_1.orientation.w = quaternion[3]

    ur5_pose_2 = geometry_msgs.msg.Pose()
    ur5_pose_2.position.x = 0.56
    ur5_pose_2.position.y = -0.03
    ur5_pose_2.position.z = 0.82
    ur5_pose_2.orientation.x = quaternion[0]
    ur5_pose_2.orientation.y = quaternion[1]
    ur5_pose_2.orientation.z = quaternion[2]
    ur5_pose_2.orientation.w = quaternion[3]

    ur5_pose_3 = geometry_msgs.msg.Pose()
    ur5_pose_3.position.x = 0.54
    ur5_pose_3.position.y = -0.24
    ur5_pose_3.position.z = 1.0
    ur5_pose_3.orientation.x = 0.635613875737
    ur5_pose_3.orientation.y = 0.77190802743
    ur5_pose_3.orientation.z = 0.00233308772292
    ur5_pose_3.orientation.w = 0.0121472162087

    ur5.go_to_pose(ur5_pose_1)


    ur5.go_to_pose(ur5_pose_2)

    rospy.sleep(1)

    ur5.attach_box("soap.dae_0")

    gr.go_to_predefined_pose("close")

    rospy.sleep(1)

    ur5.go_to_pose(ur5_pose_1)
    

    rospy.sleep(1)

    # while not rospy.is_shutdown():
    #     # ur5.go_to_pose(ur5_pose_1)
    #     # rospy.sleep(2)
    #     # ur5.go_to_pose(ur5_pose_2)
    #     # rospy.sleep(1)
    #     # gr.go_to_predefined_pose("close")
    #     # rospy.sleep(1)
    # # ur5.go_to_pose(ur5_pose_3)
    # # rospy.sleep(2)

    del ur5


if __name__ == '__main__':
    main()
