#! /usr/bin/env python

import rospy
import sys
import copy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import actionlib
import math
import tf.transformations



class Ur5Moveit:

    # Constructor
    def __init__(self):

        rospy.init_node('node_eg4_set_joint_angles', anonymous=True)

        self._planning_group = "ur5_1_planning_group"
        self._commander = moveit_commander.roscpp_initialize(sys.argv)
        self._robot = moveit_commander.RobotCommander()
        self._scene = moveit_commander.PlanningSceneInterface()
        self._group = moveit_commander.MoveGroupCommander(self._planning_group)
        self._display_trajectory_publisher = rospy.Publisher(
            '/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)

        self._exectute_trajectory_client = actionlib.SimpleActionClient(
            'execute_trajectory', moveit_msgs.msg.ExecuteTrajectoryAction)
        self._exectute_trajectory_client.wait_for_server()

        self._planning_frame = self._group.get_planning_frame()
        self._eef_link = self._group.get_end_effector_link()
        self._group_names = self._robot.get_group_names()
        self._box_name = ''

        # Current State of the Robot is needed to add box to planning scene
        self._curr_state = self._robot.get_current_state()

        rospy.loginfo(
            '\033[94m' + "Planning Group: {}".format(self._planning_frame) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "End Effector Link: {}".format(self._eef_link) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "Group Names: {}".format(self._group_names) + '\033[0m')

        rospy.loginfo('\033[94m' + " >>> Ur5Moveit init done." + '\033[0m')

    def go_to_pose(self, arg_pose):

        pose_values = self._group.get_current_pose().pose
        rospy.loginfo('\033[94m' + ">>> Current Pose:" + '\033[0m')
        rospy.loginfo(pose_values)

        self._group.set_pose_target(arg_pose)
        flag_plan = self._group.go(wait=True)  # wait=False for Async Move

        pose_values = self._group.get_current_pose().pose
        rospy.loginfo('\033[94m' + ">>> Final Pose:" + '\033[0m')
        rospy.loginfo(pose_values)

        list_joint_values = self._group.get_current_joint_values()
        rospy.loginfo('\033[94m' + ">>> Final Joint Values:" + '\033[0m')
        rospy.loginfo(list_joint_values)

        if (flag_plan == True):
            rospy.loginfo(
                '\033[94m' + ">>> go_to_pose() Success" + '\033[0m')
        else:
            rospy.logerr(
                '\033[94m' + ">>> go_to_pose() Failed. Solution for Pose not Found." + '\033[0m')

        return flag_plan

    # Destructor
    def __del__(self):
        moveit_commander.roscpp_shutdown()
        rospy.loginfo(
            '\033[94m' + "Object of class Ur5Moveit Deleted." + '\033[0m')





class gripper:

    # Constructor
    def __init__(self):


        self._planning_group = "gripper"
        self._commander = moveit_commander.roscpp_initialize(sys.argv)
        self._robot = moveit_commander.RobotCommander()
        self._scene = moveit_commander.PlanningSceneInterface()
        self._group = moveit_commander.MoveGroupCommander(self._planning_group)
        self._display_trajectory_publisher = rospy.Publisher(
            '/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)

        self._exectute_trajectory_client = actionlib.SimpleActionClient('execute_trajectory', moveit_msgs.msg.ExecuteTrajectoryAction)
        self._exectute_trajectory_client.wait_for_server()

        self._planning_frame = self._group.get_planning_frame()
        self._eef_link = self._group.get_end_effector_link()
        self._group_names = self._robot.get_group_names()


        rospy.loginfo(
            '\033[94m' + "Planning Group: {}".format(self._planning_frame) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "End Effector Link: {}".format(self._eef_link) + '\033[0m')
        rospy.loginfo(
            '\033[94m' + "Group Names: {}".format(self._group_names) + '\033[0m')

        rospy.loginfo('\033[94m' + " >>> Ur5Moveit init done." + '\033[0m')

    def go_to_predefined_pose(self, arg_pose_name):
        rospy.loginfo('\033[94m' + "Going to Pose: {}".format(arg_pose_name) + '\033[0m')
        self._group.set_named_target(arg_pose_name)
        plan = self._group.plan()
        goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
        goal.trajectory = plan
        self._exectute_trajectory_client.send_goal(goal)
        self._exectute_trajectory_client.wait_for_result()
        rospy.loginfo('\033[94m' + "Now at Pose: {}".format(arg_pose_name) + '\033[0m')
    

    # Destructor
    def __del__(self):
        moveit_commander.roscpp_shutdown()
        rospy.loginfo(
            '\033[94m' + "Object of class Ur5Moveit Deleted." + '\033[0m')



# To pick objects from specific location and orientation
def pick_object(x, y, z, roll, pitch, yaw, box_name):

    ur5 = Ur5Moveit()
    gr = gripper()
    quaternion = tf.transformations.quaternion_from_euler(roll, pitch, yaw, axes='sxyz')

    ur5_pose = geometry_msgs.msg.Pose()
    ur5_pose.position.x = x
    ur5_pose.position.y = x
    ur5_pose.position.z = z + 0.15
    ur5_pose.orientation.x = quaternion[0]
    ur5_pose.orientation.y = quaternion[1]
    ur5_pose.orientation.z = quaternion[2]
    ur5_pose.orientation.w = quaternion[3]

    ur5.go_to_pose(ur5_pose)
    rospy.sleep(1)

    ur5_pose.position.z = z

    ur5.go_to_pose(ur5_pose)
    rospy.sleep(1)

    gr.go_to_predefined_pose("close")

    del ur5
    del gr


# To drop objects at specific box
def drop_object(box_color):
    ur5 = Ur5Moveit()
    gr = gripper()

    if box_color == "red"
        quaternion = tf.transformations.quaternion_from_euler(-1.57, -0.02, -0.9, axes='sxyz')
        x = 
        y =
        z =   

    elif box_color == "blue"
        quaternion = tf.transformations.quaternion_from_euler(-1.57, -0.02, -0.9, axes='sxyz')
        x = 
        y = 
        z = 

    ur5_pose = geometry_msgs.msg.Pose()
    ur5_pose.position.x = x
    ur5_pose.position.y = x
    ur5_pose.position.z = z
    ur5_pose.orientation.x = quaternion[0]
    ur5_pose.orientation.y = quaternion[1]
    ur5_pose.orientation.z = quaternion[2]
    ur5_pose.orientation.w = quaternion[3]

    ur5.go_to_pose(ur5_pose)
    rospy.sleep(1)
    gr.go_to_predefined_pose("open")

    del ur5
    del gr


quaternion1 = tf.transformations.quaternion_from_euler(-math.pi/2, 0, -0.9, axes='sxyz')
quaternion2 = tf.transformations.quaternion_from_euler(-1.57, -0.02, -0.9, axes='sxyz')
quaternion3 = tf.transformations.quaternion_from_euler(-1.58732042002, 0.00999523051594, 2.32011439703, axes='sxyz')
quaternion4 = tf.transformations.quaternion_from_euler(-1.58796064133, 0.0105114864675, 2.32575802448, axes='sxyz')
quaternion5 = tf.transformations.quaternion_from_euler(-1.64570841473, 0.0178570746973, 2.8712563524, axes='sxyz')

def main():
    

    ur5 = Ur5Moveit()
    gr = gripper()

    # ur5._scene.get_known_object_names()

    ur5_pose_1 = geometry_msgs.msg.Pose()
    ur5_pose_1.position.x = 0.56
    ur5_pose_1.position.y = -0.03
    ur5_pose_1.position.z = 1.0
    ur5_pose_1.orientation.x = quaternion1[0]
    ur5_pose_1.orientation.y = quaternion1[1]
    ur5_pose_1.orientation.z = quaternion1[2]
    ur5_pose_1.orientation.w = quaternion1[3]

    ur5_pose_2 = geometry_msgs.msg.Pose()
    ur5_pose_2.position.x = 0.56
    ur5_pose_2.position.y = -0.03
    ur5_pose_2.position.z = 0.82
    ur5_pose_2.orientation.x = quaternion1[0]
    ur5_pose_2.orientation.y = quaternion1[1]
    ur5_pose_2.orientation.z = quaternion1[2]
    ur5_pose_2.orientation.w = quaternion1[3]

    ur5_pose_3 = geometry_msgs.msg.Pose()
    ur5_pose_3.position.x = -0.1
    ur5_pose_3.position.y = 0.675
    ur5_pose_3.position.z = 1.1
    ur5_pose_3.orientation.x = quaternion2[0]
    ur5_pose_3.orientation.y = quaternion2[1]
    ur5_pose_3.orientation.z = quaternion2[2]
    ur5_pose_3.orientation.w = quaternion2[3]

    ur5_pose_4 = geometry_msgs.msg.Pose()
    ur5_pose_4.position.x = 0.451941926729 
    ur5_pose_4.position.y = 0.226053974336
    ur5_pose_4.position.z = 1.0
    ur5_pose_4.orientation.x = quaternion3[0]
    ur5_pose_4.orientation.y = quaternion3[1]
    ur5_pose_4.orientation.z = quaternion3[2]
    ur5_pose_4.orientation.w = quaternion3[3]

    ur5_pose_5 = geometry_msgs.msg.Pose()
    ur5_pose_5.position.x = -0.00785030192588 
    ur5_pose_5.position.y = -0.698264176274 
    ur5_pose_5.position.z = 1.2
    ur5_pose_5.orientation.x = quaternion4[0]
    ur5_pose_5.orientation.y = quaternion4[1]
    ur5_pose_5.orientation.z = quaternion4[2]
    ur5_pose_5.orientation.w = quaternion4[3]

    ur5_pose_6 = geometry_msgs.msg.Pose()
    ur5_pose_6.position.x = 0.55602198468 
    ur5_pose_6.position.y = -0.26241120349 
    ur5_pose_6.position.z = 0.872938024944
    ur5_pose_6.orientation.x = quaternion5[0]
    ur5_pose_6.orientation.y = quaternion5[1]
    ur5_pose_6.orientation.z = quaternion5[2]
    ur5_pose_6.orientation.w = quaternion5[3]   


    box1_name = "soap.dae_0"
    box2_name = "soap.dae_2"
    box3_name = "biscuits.dae_0"

    gr.go_to_predefined_pose("open")

    ur5.go_to_pose(ur5_pose_1)
    rospy.sleep(1)

    ur5.go_to_pose(ur5_pose_2)
    rospy.sleep(1)

    # gr._scene.remove_attached_object(gr._eef_link, name=box_name)

    # touch_links = gr._robot.get_link_names(group=gr._planning_group)

    # gr._scene.attach_box(gr._eef_link, box_name, touch_links=touch_links)
    # rospy.sleep(1)

    # gr._scene.remove_attached_object(gr._eef_link, name=box_name)

    gr._scene.remove_world_object(box1_name)

    gr.go_to_predefined_pose("close")
    rospy.sleep(1)

    ur5.go_to_pose(ur5_pose_1)
    rospy.sleep(1)

    ur5.go_to_pose(ur5_pose_3)
    rospy.sleep(1)

    gr.go_to_predefined_pose("open")
    rospy.sleep(1)

    ur5.go_to_pose(ur5_pose_4)
    rospy.sleep(1)
    ur5_pose_4.position.z = 0.82

    ur5.go_to_pose(ur5_pose_4)
    rospy.sleep(1)

    gr._scene.remove_world_object(box2_name)

    gr.go_to_predefined_pose("close")
    rospy.sleep(1)

    ur5.go_to_pose(ur5_pose_5)
    rospy.sleep(1)

    gr.go_to_predefined_pose("open")
    rospy.sleep(1)

    ur5.go_to_pose(ur5_pose_6)
    rospy.sleep(1)

    gr._scene.remove_world_object(box3_name)

    gr.go_to_predefined_pose("close")
    rospy.sleep(1)

    ur5.go_to_pose(ur5_pose_5)
    rospy.sleep(1)

    gr.go_to_predefined_pose("open")
    rospy.sleep(1)



    # gr._scene.remove_attached_object(gr._eef_link, name=box_name)

    # rospy.sleep(1)
    
    # gr.go_to_predefined_pose("open")

    # rospy.sleep(1)

    # gr._scene.remove_world_object(box_name)


    

    # while not rospy.is_shutdown():
    #     # ur5.go_to_pose(ur5_pose_1)
    #     # rospy.sleep(2)
    #     ur5.go_to_pose(ur5_pose_2)
    #     rospy.sleep(1)
    #     # gr.go_to_predefined_pose("close")
    #     # rospy.sleep(1)
    # # ur5.go_to_pose(ur5_pose_3)
    # # rospy.sleep(2)

    del ur5
    del gr


if __name__ == '__main__':
    main()